Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gnote
Source: https://download.gnome.org/sources/gnote/

Files: *
Copyright: 2004-2007 Alex Graveley <alex@beatniksoftware.com>
           2009, Hubert Figuiere <hub@figuiere.net>
           2010-2018 Aurimas Černius <aurisc4@gmail.com>
           2009-2011 Debarshi Ray <debarshir@src.gnome.org>
License: GPL-3.0+

Files: help/*
Copyright: 2008, Alex Graveley
           2008, Brent Smith
           2008, Free Software Foundation
           2008, Boyd Timothy
           2008, Sandy Armstrong
           2008, Paul Cutler
           2009, Hubert Figuiere
License: GFDL-1.1+

Files: src/sharp/*.cpp
       src/sharp/*.hpp
Copyright: 2009, Hubert Figuiere <hub@figuiere.net>
           2010-2014 Aurimas Černius <aurisc4@gmail.com>
License: Expat

Files: src/actionmanager.cpp
Copyright: 2009, Hubert Figuiere <hub@figuiere.net>
           2005-2006, Novell, Inc.
License: GPL-3.0+ and Expat

Files: debian/*
Copyright: 2009, Robert Millan <rmh.debian@aybabtu.com>
           2011, Vincent Cheng <vcheng@debian.org>
License: GPL-3.0+

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GFDL-1.1+
 Permission is granted to copy, distribute and/or modify this
 document under the terms of the GNU Free Documentation
 License (GFDL), Version 1.1 or any later version published
 by the Free Software Foundation with no Invariant Sections,
 no Front-Cover Texts, and no Back-Cover Texts.
 .
 On Debian systems, the complete text of the GNU Free Documentation License
 version 1.2 can be found in "/usr/share/common-licenses/GFDL-1.2".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
